# This suite runs tests with --disable-blink-features=ShadowRootClonable.

This is mainly intended to make sure that things still work correctly when the clonable flag is disabled.

