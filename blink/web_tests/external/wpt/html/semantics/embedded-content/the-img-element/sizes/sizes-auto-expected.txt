This is a testharness.js-based test.
Found 8 FAIL, 0 TIMEOUT, 0 NOTRUN.
[FAIL] <img loading="lazy" sizes="auto" data-ref="ref2" srcset="/images/green-1x1.png?img14 50w, /images/green-16x16.png?img14 51w">
  assert_equals: expected "http://web-platform.test:8001/images/green-16x16.png" but got "http://web-platform.test:8001/images/green-1x1.png"
[FAIL] <picture><source srcset="/images/green-1x1.png?picture14 50w, /images/green-16x16.png?picture14 51w"><img loading="lazy" sizes="auto" data-ref="ref2"></picture>
  assert_equals: expected "http://web-platform.test:8001/images/green-16x16.png" but got "http://web-platform.test:8001/images/green-1x1.png"
[FAIL] <img loading="lazy" sizes="auto" style="min-inline-size: 10px" data-ref="ref2" srcset="/images/green-1x1.png?img25 50w, /images/green-16x16.png?img25 51w">
  assert_equals: expected "http://web-platform.test:8001/images/green-16x16.png" but got "http://web-platform.test:8001/images/green-1x1.png"
[FAIL] <picture><source srcset="/images/green-1x1.png?picture25 50w, /images/green-16x16.png?picture25 51w"><img loading="lazy" sizes="auto" style="min-inline-size: 10px" data-ref="ref2"></picture>
  assert_equals: expected "http://web-platform.test:8001/images/green-16x16.png" but got "http://web-platform.test:8001/images/green-1x1.png"
[FAIL] <img loading="lazy" sizes="auto" style="min-block-size: 10px; writing-mode: vertical-rl" data-ref="ref2" srcset="/images/green-1x1.png?img29 50w, /images/green-16x16.png?img29 51w">
  assert_equals: expected "http://web-platform.test:8001/images/green-16x16.png" but got "http://web-platform.test:8001/images/green-1x1.png"
[FAIL] <picture><source srcset="/images/green-1x1.png?picture29 50w, /images/green-16x16.png?picture29 51w"><img loading="lazy" sizes="auto" style="min-block-size: 10px; writing-mode: vertical-rl" data-ref="ref2"></picture>
  assert_equals: expected "http://web-platform.test:8001/images/green-16x16.png" but got "http://web-platform.test:8001/images/green-1x1.png"
[FAIL] <img loading="lazy" sizes="auto" style="position: absolute; left: 50%; right: 49%" data-ref="ref2" srcset="/images/green-1x1.png?img34 50w, /images/green-16x16.png?img34 51w">
  assert_equals: expected "http://web-platform.test:8001/images/green-16x16.png" but got "http://web-platform.test:8001/images/green-1x1.png"
[FAIL] <picture><source srcset="/images/green-1x1.png?picture34 50w, /images/green-16x16.png?picture34 51w"><img loading="lazy" sizes="auto" style="position: absolute; left: 50%; right: 49%" data-ref="ref2"></picture>
  assert_equals: expected "http://web-platform.test:8001/images/green-16x16.png" but got "http://web-platform.test:8001/images/green-1x1.png"
Harness: the test ran to completion.

