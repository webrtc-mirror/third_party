This is a testharness.js-based test.
[FAIL] e.style['transition'] = "1s" should set the property value
  assert_equals: serialization should be canonical expected "1s" but got "all 1s ease 0s"
[FAIL] e.style['transition'] = "cubic-bezier(0, -2, 1, 3)" should set the property value
  assert_equals: serialization should be canonical expected "cubic-bezier(0, -2, 1, 3)" but got "all 0s cubic-bezier(0, -2, 1, 3) 0s"
[FAIL] e.style['transition'] = "1s -3s" should set the property value
  assert_equals: serialization should be canonical expected "1s -3s" but got "all 1s ease -3s"
[FAIL] e.style['transition'] = "none" should set the property value
  assert_equals: serialization should be canonical expected "none" but got "none 0s ease 0s"
[FAIL] e.style['transition'] = "top" should set the property value
  assert_equals: serialization should be canonical expected "top" but got "top 0s ease 0s"
[FAIL] e.style['transition'] = "1s -3s, cubic-bezier(0, -2, 1, 3) top" should set the property value
  assert_equals: serialization should be canonical expected "1s -3s, top cubic-bezier(0, -2, 1, 3)" but got "all 1s ease -3s, top 0s cubic-bezier(0, -2, 1, 3) 0s"
[FAIL] e.style['transition'] = "all" should set the property value
  assert_equals: serialization should be canonical expected "all" but got "all 0s ease 0s"
[FAIL] e.style['transition'] = "all 1s" should set the property value
  assert_equals: serialization should be canonical expected "1s" but got "all 1s ease 0s"
Harness: the test ran to completion.

