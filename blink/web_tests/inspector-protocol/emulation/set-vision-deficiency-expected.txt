Tests that vision deficiencies can be emulated.
<p>Emulating "none":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFRJREFUWIXt18ENACAMw8DA5N0chnCQePgGiKz+ujI5aZru3K6uPWAgZSBlIGUgZSBlIGUgZSBlILWS030iyr6/oIGUgZSBlIGUgZSBlIGUgZSB1AXNwwVLtuf+dAAAAABJRU5ErkJggg==">
<p>Emulating "achromatopsia":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFdJREFUWIXt18ENACAMw0BArNuZOwoM4SDx8A0QWf11VtUZQd2dnBsruvaAgZSBlIGUgZSBlIGUgZSBlIHUTv8Qad9f0EDKQMpAykDKQMpAykDKQMpA6gIVDwfSja9TnQAAAABJRU5ErkJggg==">
<p>Emulating "blurredVision":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAj5JREFUWIXtlstu2zAQRQ8flqz41QRG4q/oT/e7ujSQVZDAimwxFNnF0LFTtLFaKoAXusAAAiwNji8fcxXff0SWW5g+QdGA8qAjEPlcM4glsAJ/D90DHDZQr2G3gn0FzxbQ8Kou9Pq7LMst3P6E+SMUNRgHKkp9qjnEKcQN+D04JVBmBuEGQglVhMN/syXA6gkWj7DYwvQFtAMVeji4gLiU9/wE3C2YBnwLhw6aAAqpLMBJA5MayheYPgug7gPYQYwQ5/DWAAcoHNgObIBJHtgJUHuwTpZWOzBtT8ACYisVPFgPJoCJybWY7Z4AqsB76VQq9NiDAWIQF3WCUcNAfQR8VzyV6nOK4x+K357zpQfr9EUaAXM1AuZqBMzV2T24kPFFIZfwxbusSu+aL/2fFu6ANUSfZmsrE6LXqJtCnEA8QioGSQgfAOMKwibBVSfQi4AmwS3ku1BAMBCObg4DaenuodtDV0JXy+DvFVh1cq4CfwfdTHocIf1QgP4BWgXtrUQm49OWugSoICqIBfgZtGtwc3BTcHZABw8bScK6gbeUBXXPYR+ULLUv4W0Or9+kV2vBa9jnQ1rqtcR030rYNH3i/lFKljMYcW5fwesNHAqojfy+y4O07FbAjcR0G05hsy8gCGRnobEC11hxLxMuAVbgSigTnP3HLHc8DF6D0+BMWlo9yD5UzHykOkvCOT336mxZhzkkCoJYtsjstDtrOaBUupWvVlcfFkbAXI2AuRoBczUC5moEzNUImKurB/wFQkfph8QDq7cAAAAASUVORK5CYII=">
<p>Emulating "reducedContrast":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFhJREFUWIXt18EJACAQxEAVS7NrWxEsRYvICj4yBSzhflfnmqck7RGda9G1BwykDKQMpAykDKQMpAykDKQMpHr6h0j7/oIGUgZSBlIGUgZSBlIGUgZSBlIXEeYF6dOYQhsAAAAASUVORK5CYII=">
<p>Emulating "none":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFRJREFUWIXt18ENACAMw8DA5N0chnCQePgGiKz+ujI5aZru3K6uPWAgZSBlIGUgZSBlIGUgZSBlILWS030iyr6/oIGUgZSBlIGUgZSBlIGUgZSB1AXNwwVLtuf+dAAAAABJRU5ErkJggg==">
<p>Emulating "deuteranopia":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAGNJREFUWIXt10ENgDAQBdFtVWAGQzVQDdWHBWSQgIj5JD3ME/Az2du2OY63gtZ1J+eqR9d+YCBlIGUgZSBlIGUgZSBlIGUg1ep8oj9J2vYXNJAykDKQMpAykDKQMpAykDKQ+gBGJwa/n9seWgAAAABJRU5ErkJggg==">
<p>Emulating "none":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFRJREFUWIXt18ENACAMw8DA5N0chnCQePgGiKz+ujI5aZru3K6uPWAgZSBlIGUgZSBlIGUgZSBlILWS030iyr6/oIGUgZSBlIGUgZSBlIGUgZSB1AXNwwVLtuf+dAAAAABJRU5ErkJggg==">
<p>Emulating "protanopia":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFxJREFUWIXt17ENACEMBEFDl7RB/hHFUgYUsYf0wU4Bp5UztzXrVNC3o3PVo2sPGEgZSBlIGUgZSBlIGUgZSBlItRon+0SE/f6CBlIGUgZSBlIGUgZSBlIGUgZSF8y1Bv1/Sx2kAAAAAElFTkSuQmCC">
<p>Emulating "tritanopia":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFlJREFUWIXt10ENACAQxEAgqEEbEvCMBBDRJeHREbBp7ne1rHlK0h7RuRZde8BAykDKQMpAykDKQMpAykDKQKqnf4i07y9oIGUgZSBlIGUgZSBlIGUgZSB1AQm3BGtcwwkhAAAAAElFTkSuQmCC">
<p>Emulating "tritanopia":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFlJREFUWIXt10ENACAQxEAgqEEbEvCMBBDRJeHREbBp7ne1rHlK0h7RuRZde8BAykDKQMpAykDKQMpAykDKQKqnf4i07y9oIGUgZSBlIGUgZSBlIGUgZSB1AQm3BGtcwwkhAAAAAElFTkSuQmCC">
<p>Emulating "some-invalid-deficiency":
{
  "code": -32602,
  "message": "Unknown vision deficiency type"
}
<p>Emulating "":
{
  "code": -32602,
  "message": "Unknown vision deficiency type"
}
<p>Emulating "achromatomaly":
{
  "code": -32602,
  "message": "Unknown vision deficiency type"
}
<p>Emulating "deuteranomaly":
{
  "code": -32602,
  "message": "Unknown vision deficiency type"
}
<p>Emulating "protanomaly":
{
  "code": -32602,
  "message": "Unknown vision deficiency type"
}
<p>Emulating "tritanomaly":
{
  "code": -32602,
  "message": "Unknown vision deficiency type"
}
<p>Navigating&mldr;
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFlJREFUWIXt10ENACAQxEAgqEEbEvCMBBDRJeHREbBp7ne1rHlK0h7RuRZde8BAykDKQMpAykDKQMpAykDKQKqnf4i07y9oIGUgZSBlIGUgZSBlIGUgZSB1AQm3BGtcwwkhAAAAAElFTkSuQmCC">
<p>Emulating "achromatopsia":
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAFdJREFUWIXt18ENACAMw0BArNuZOwoM4SDx8A0QWf11VtUZQd2dnBsruvaAgZSBlIGUgZSBlIGUgZSBlIHUTv8Qad9f0EDKQMpAykDKQMpAykDKQMpA6gIVDwfSja9TnQAAAABJRU5ErkJggg==">

