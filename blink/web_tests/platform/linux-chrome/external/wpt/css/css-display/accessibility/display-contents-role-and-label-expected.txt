This is a testharness.js-based test.
Found 23 FAIL, 0 TIMEOUT, 0 NOTRUN.
[FAIL] Label: listitem within ul with display: contents, as child of div with display: grid, has listitem role
  assert_equals: <li data-expectedrole="listitem" data-expectedlabel="x" data-testname="listitem within ul with display: contents, as child of div with display: grid, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">x</li> expected "x" but got ""
[FAIL] Label: nav with display: contents and aria-label has navigation role
  assert_equals: <nav style="display: contents;" aria-label="label" data-expectedrole="navigation" data-testname="nav with display: contents and aria-label has navigation role" class="ex-role-and-label ex-label-only ex-role-only">x</nav> expected (object) null but got (string) "label"
[FAIL] Label: aside with display: contents and aria-label has complementary role
  assert_equals: <aside style="display: contents;" aria-label="label" data-expectedrole="complementary" data-testname="aside with display: contents and aria-label has complementary role" class="ex-role-and-label ex-label-only ex-role-only">x</aside> expected (object) null but got (string) "label"
[FAIL] Label: div with role navigation, aria-label and display: contents has navigation role
  assert_equals: <div role="navigation" aria-label="label" style="display: contents;" data-expectedrole="navigation" data-testname="div with role navigation, aria-label and display: contents has navigation role" class="ex-role-and-label ex-label-only ex-role-only">x</div> expected (object) null but got (string) "label"
[FAIL] Label: div with role complementary, aria-label and display: contents has complementary role
  assert_equals: <div role="complementary" aria-label="label" style="display: contents;" data-expectedrole="complementary" data-testname="div with role complementary, aria-label and display: contents has complementary role" class="ex-role-and-label ex-label-only ex-role-only">x</div> expected (object) null but got (string) "label"
[FAIL] Label: div with role search and display: contents has search role
  assert_equals: <div role="search" aria-label="label" style="display: contents;" data-expectedrole="search" data-testname="div with role search and display: contents has search role" class="ex-role-and-label ex-label-only ex-role-only">x</div> expected (object) null but got (string) "label"
[FAIL] Label: li as child of ul with role list, both with display: contents, has listitem role
  assert_equals: <li style="display: contents;" data-expectedrole="listitem" data-expectedlabel="x" data-testname="li as child of ul with role list, both with display: contents, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">x</li> expected "x" but got ""
[FAIL] Label: li, as child of ul with role list and display: contents, has listitem role
  assert_equals: <li data-expectedrole="listitem" data-expectedlabel="y" data-testname="li, as child of ul with role list and display: contents, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">y</li> expected "y" but got ""
[FAIL] Label: li with display: contents, as child of ul with role list, has listitem role
  assert_equals: <li style="display: contents;" data-expectedrole="listitem" data-expectedlabel="y" data-testname="li with display: contents, as child of ul with role list, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">y</li> expected "y" but got ""
[FAIL] Label: li as child of ol with role list, both with display: contents, has listitem role
  assert_equals: <li style="display: contents;" data-expectedrole="listitem" data-expectedlabel="x" data-testname="li as child of ol with role list, both with display: contents, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">x</li> expected "x" but got ""
[FAIL] Label: li, as child of ol with role list and display: contents, has listitem role
  assert_equals: <li data-expectedrole="listitem" data-expectedlabel="y" data-testname="li, as child of ol with role list and display: contents, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">y</li> expected "y" but got ""
[FAIL] Label: li with display: contents, as child of ol with role list, has listitem role
  assert_equals: <li style="display: contents;" data-expectedrole="listitem" data-expectedlabel="y" data-testname="li with display: contents, as child of ol with role list, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">y</li> expected "y" but got ""
[FAIL] Label: div with listitem role, as child of div with display: contents, has listitem role
  assert_equals: <div role="listitem" data-expectedrole="listitem" data-expectedlabel="x" data-testname="div with listitem role, as child of div with display: contents, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">x</div> expected "x" but got ""
[FAIL] Label: div with listitem role (as child of div with list role), both with display: contents, has listitem role
  assert_equals: <div role="listitem" style="display: contents;" data-expectedrole="listitem" data-expectedlabel="y" data-testname="div with listitem role (as child of div with list role), both with display: contents, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">y</div> expected "y" but got ""
[FAIL] Label: div with listitem role with display: contents, as child of div with list role, has listitem role
  assert_equals: <div role="listitem" style="display: contents;" data-expectedrole="listitem" data-expectedlabel="y" data-testname="div with listitem role with display: contents, as child of div with list role, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">y</div> expected "y" but got ""
[FAIL] Role: g element with display: contents, as child of svg, is labelled via title element
  assert_equals: <g style="display: contents;" fill="silver" stroke="blue" data-expectedlabel="group label" data-expectedrole="group" data-testname="g element with display: contents, as child of svg, is labelled via title element" class="ex-role-and-label ex-label-only ex-role-only">\n      <title>group label</title>\n      <circle cx="20" cy="20" r="10"></circle>\n      <circle cx="20" cy="20" r="10"></circle>\n    </g> expected "group" but got "generic"
[FAIL] Role: td as child of tr with display: contents, within table with display: flex, has cell role
  assert_equals: <td data-expectedrole="cell" data-expectedlabel="x" data-testname="td as child of tr with display: contents, within table with display: flex, has cell role" class="ex-role-and-label ex-label-only ex-role-only">x</td> expected "cell" but got "gridcell"
[FAIL] Role: td as child of tr with display: contents, within table with role=table with display: flex, has cell role
  assert_equals: <td data-expectedrole="cell" data-expectedlabel="x" data-testname="td as child of tr with display: contents, within table with role=table with display: flex, has cell role" class="ex-role-and-label ex-label-only ex-role-only">x</td> expected "cell" but got "gridcell"
[FAIL] Role: li as child of ul with role list, both with display: contents, has listitem role
  assert_equals: <li style="display: contents;" data-expectedrole="listitem" data-expectedlabel="x" data-testname="li as child of ul with role list, both with display: contents, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">x</li> expected "listitem" but got "none"
[FAIL] Role: li with display: contents, as child of ul with role list, has listitem role
  assert_equals: <li style="display: contents;" data-expectedrole="listitem" data-expectedlabel="y" data-testname="li with display: contents, as child of ul with role list, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">y</li> expected "listitem" but got "none"
[FAIL] Role: li as child of ol with role list, both with display: contents, has listitem role
  assert_equals: <li style="display: contents;" data-expectedrole="listitem" data-expectedlabel="x" data-testname="li as child of ol with role list, both with display: contents, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">x</li> expected "listitem" but got "none"
[FAIL] Role: li with display: contents, as child of ol with role list, has listitem role
  assert_equals: <li style="display: contents;" data-expectedrole="listitem" data-expectedlabel="y" data-testname="li with display: contents, as child of ol with role list, has listitem role" class="ex-role-and-label ex-label-only ex-role-only">y</li> expected "listitem" but got "none"
[FAIL] Role: td within tr in table with role table, all with display: contents, has cell role
  assert_equals: <td style="display: contents;" data-expectedrole="cell" data-expectedlabel="x" data-testname="td within tr in table with role table, all with display: contents, has cell role" class="ex-role-and-label ex-label-only ex-role-only">x</td> expected "cell" but got "gridcell"
Harness: the test ran to completion.
