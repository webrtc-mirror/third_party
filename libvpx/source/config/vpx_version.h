// This file is generated. Do not edit.
#define VERSION_MAJOR  1
#define VERSION_MINOR  8
#define VERSION_PATCH  0
#define VERSION_EXTRA  "575-g30e7f9d85"
#define VERSION_PACKED ((VERSION_MAJOR<<16)|(VERSION_MINOR<<8)|(VERSION_PATCH))
#define VERSION_STRING_NOSP "v1.8.0-575-g30e7f9d85"
#define VERSION_STRING      " v1.8.0-575-g30e7f9d85"
