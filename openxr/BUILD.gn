# Copyright 2019 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//device/vr/buildflags/buildflags.gni")

if (enable_openxr) {
  component("openxr") {
    output_name = "openxr_loader"

    sources = [
      "gen/xr_generated_dispatch_table.c",
      "gen/xr_generated_dispatch_table.h",
      "gen/xr_generated_utilities.c",
      "gen/xr_generated_utilities.h",
      "openxr.h",
      "openxr_platform.h",
      "openxr_platform_defines.h",
      "src/src/common/filesystem_utils.cpp",
      "src/src/common/filesystem_utils.hpp",
      "src/src/loader/api_layer_interface.cpp",
      "src/src/loader/api_layer_interface.hpp",
      "src/src/loader/loader.rc",
      "src/src/loader/loader_core.cpp",
      "src/src/loader/loader_instance.cpp",
      "src/src/loader/loader_instance.hpp",
      "src/src/loader/loader_logger.cpp",
      "src/src/loader/loader_logger.hpp",
      "src/src/loader/manifest_file.cpp",
      "src/src/loader/manifest_file.hpp",
      "src/src/loader/runtime_interface.cpp",
      "src/src/loader/runtime_interface.hpp",
    ]

    include_dirs = [
      "gen",
      "include",
      "src/include",
      "src/src/common",
    ]

    deps = [
      "//base",
      "//third_party/jsoncpp",
    ]

    public_configs = [ ":config" ]

    configs += [ "//build/config/compiler:exceptions" ]

    configs -= [
      # TODO(crbug/963541): Currently OpenXR loader library is built with
      # exceptions,
      # but the third_party/jsconcpp it depends on is built without using
      # exceptions. Fix the bug and remove this config +=
      # ["//build/config/compiler:exceptions"] and
      # configs -= ["//build/config/compiler:no_exceptions"].
      # and probably deps = ["//base"]
      "//build/config/compiler:no_exceptions",

      # lean_and_mean is defined in the openxr file and some parent build.gn
      # file, need to remove it to avoid compiler error
      "//build/config/win:lean_and_mean",
      "//build/config/compiler:chromium_code",
    ]

    defines = [
      "OPENXR_NON_CMAKE_BUILD",
      "DISABLE_STD_FILESYSTEM",
    ]

    if (is_component_build) {
      defines += [ "XRAPI_DLL_EXPORT" ]
    }

    cflags_cc = [
      "-Wno-format",
      "-Wno-microsoft-cast",
      "-Wno-microsoft-include",
    ]
  }

  config("config") {
    # These build define flags need to be set for openxr.h to define
    # constants related to Windows and D3D.
    if (is_win) {
      defines = [
        "XR_OS_WINDOWS",
        "XR_USE_PLATFORM_WIN32",
        "XR_USE_GRAPHICS_API_D3D11",
      ]
    }
  }
}
